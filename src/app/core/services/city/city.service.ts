import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { City } from '../../models/city';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  constructor(private http: HttpClient) {}

  /**
   * Get cities from an input value
   * @param value search value
   */
  getCities(value?: string): Observable<City[]> {
    return this.http.get('./assets/files/city.list.json').pipe(
      map((cities: City[]) => {
        return cities
          .filter(city =>
            city.name.toLowerCase().includes(value.toLowerCase())
          )
          .slice(0, 10);
      })
    );
  }
}
