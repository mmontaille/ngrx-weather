import { Component, OnInit, Input } from '@angular/core';
import { City } from 'app/core/models/city';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  @Input() city: City;
  @Input() forecast: any;

  constructor() { }

  ngOnInit(): void {
  }

}
